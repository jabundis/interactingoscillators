This repository solves a general Hamiltonian for interacting nonlinear oscillators given by (Einstein's summation):

H = w_j a_j^\dagger a_j 
    + \kappa \sum_{<j, k>} a_j^\dagger a_k 
    + \tau (a_j^\dagger a_j)^2.


